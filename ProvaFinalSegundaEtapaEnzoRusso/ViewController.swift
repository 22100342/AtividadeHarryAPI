//
//  ViewController.swift
//  ProvaFinalSegundaEtapaEnzoRusso
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica {
    let nomeMusica: String
    let nomeAlbum: String
    let nomeCantor: String
    let nomeImagemPequena: String
    let nomeImagemGrade: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var listaDeMusicas:[Musica] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.listaDeMusicas[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.nomeImagemPequena)
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        self.tableView.delegate = self
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Pontos Cadeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valença", nomeImagemPequena: "capa_alceu_pequeno", nomeImagemGrade: "capa_alceu_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrade: "capa_zeca_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImagemPequena: "capa_adoniran_pequeno", nomeImagemGrade: "capa_adoniran_grande"))
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusicas[indice]
        
        detalhesViewController.nomeImagem = musica.nomeImagemGrade
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
    }

}

